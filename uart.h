//! \file uart.h
//! \brief Driver for the on-chip UART available on ATMEL AVR micro-controllers.

#ifndef D_UART_H
#define D_UART_H

#include <inttypes.h>

void UART_Init(const uint16_t baudRate);

void UART_Putc(const char c);

void UART_Puts(char *s);

#endif // D_UART_H

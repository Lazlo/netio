//! \file sch.h
//! \brief Co-operative task scheduler

#ifndef D_SCH_H
#define D_SCH_H

#include <inttypes.h>

//! \brief Type used for the task id.
typedef uint8_t TaskId_t;

// Initialize the scheduler.
void Sch_Init(const uint16_t tick_ms);

// Add a task to the scheduler and return a numeric task identified.
TaskId_t Sch_Add_Task(void (* fp)(void), const unsigned int delay, const unsigned int periode);

// Delete task from task list.
void Sch_Delete_Task(const TaskId_t id);

// Start the scheduler.
void Sch_Start(void);

// Let the scheduler dispatch tasks.
void Sch_Dispatch_Tasks(void);

#endif // D_SCH_H

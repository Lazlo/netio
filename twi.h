#ifndef D_TWI_H
#define D_TWI_H

#include <inttypes.h>

void TWI_Master_Init(void);

void TWI_Master_Send(uint8_t sla, uint8_t b);

#endif /* D_TWI_H */

//
// Rotary Encoder Library
//
//

#include "re.h"

static uint8_t s_last = 0;
static uint8_t s_curr = 0;

static int tell_direction(uint8_t last, uint8_t curr)
{
#	define CONFIG_RE_TELL_DIR 1

#	if CONFIG_RE_TELL_DIR == 1
#		warning "in tell_direction(): using new implementation."

	enum { ERR = 0, CW = 1, CCW = 2 };

	uint8_t trans[16] = {
		ERR,	/* 0	0000 - invalid */
		CCW,	/* 1	0001 - CCW */
		CW,	/* 2	0010 - CW */
		ERR,	/* 3	0011 - invalid */
		CW,	/* 4	0100 - CW */
		ERR,	/* 5	0101 - invalid */
		ERR,	/* 6	0110 - invalid */
		CCW,	/* 7	0111 - CCW */
		CCW,	/* 8	1000 - CCW */
		ERR,	/* 9	1001 - invalid */
		ERR,	/* 10	1010 - invalid */
		CW,	/* 11	1011 - CW */
		ERR,	/* 12	1100 - invalid */
		CW,	/* 13	1101 - CW */
		CCW,	/* 14	1110 - CCW */
		ERR	/* 15	1111 - invalid */
	};

	uint8_t tmp;

	tmp = last << 2;
	tmp += curr;

	return trans[tmp];

#	else
#		warning "in tell_direction(): using old implementation."

	if((last == 0 && curr == 2) ||
		(last == 2 && curr == 3) ||
		(last == 3 && curr == 1) ||
		(last == 1 && curr == 0)) {
		return 1;
	} else if((last == 0 && curr == 1) ||
		(last == 1 && curr == 3) ||
		(last == 3 && curr == 2) ||
		(last == 2 && curr == 0)) {
		return 2;
	} else {
		return 0;
	}

#	endif
}

void Enc_Init(void)
{
}

void Enc_Update(void)
{
}

void Enc_FeedData(uint8_t data)
{
	/* Save the current as last state. */
	s_last = s_curr;
	/* Save new data as current. */
	s_curr = data;
}

int Enc_Changed(void)
{
	if(s_last != s_curr)
		return 1;

	return 0;
}

int Enc_GetKey(void)
{
	uint8_t key = 0;

	uint8_t i;
	uint8_t last;
	uint8_t curr;

	for(i = 0; i < 2; i++)
	{
		last = s_last >> (2 * i);
		curr = s_curr >> (2 * i);

		if(last != curr)
		{
			key = tell_direction(last, curr);
			//key = 1+i;
		}
	}

	return key;
}

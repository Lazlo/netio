#ifndef D_PC_LINK_0_H
#define D_PC_LINK_0_H

void link_Init(void (fptr_putc(char)));
void link_Update(void);
void link_Putc(const char c);
void link_Puts(const char *s);

#endif // D_PC_LINK_0_H

#ifndef D_GPIO_H
#define D_GPIO_H

#include <inttypes.h>

void GPIO_Init(void);

uint8_t GPIO_ReadInputs(void);

void GPIO_WriteOutputs(uint8_t byte);

#endif // D_GPIO_H

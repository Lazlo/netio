///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#include "pins.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#define GPIO_RE1A_AS_INPUT()	(GPIO_RE1A_DDR &= ~(1 << GPIO_RE1A))
#define GPIO_RE1A_PULLUP_ENABLE()	(GPIO_RE1A_PORT |= (1 << GPIO_RE1A))
#define GPIO_RE1A_READ()	(GPIO_RE1A_PIN & (1 << GPIO_RE1A))

#define GPIO_RE1B_AS_INPUT()	(GPIO_RE1B_DDR &= ~(1 << GPIO_RE1B))
#define GPIO_RE1B_PULLUP_ENABLE()	(GPIO_RE1B_PORT |= (1 << GPIO_RE1B))
#define GPIO_RE1B_READ()	(GPIO_RE1B_PIN & (1 << GPIO_RE1B))

#define GPIO_RE2A_AS_INPUT()	(GPIO_RE2A_DDR &= ~(1 << GPIO_RE2A))
#define GPIO_RE2A_PULLUP_ENABLE()	(GPIO_RE2A_PORT |= (1 << GPIO_RE2A))
#define GPIO_RE2A_READ()	(GPIO_RE2A_PIN & (1 << GPIO_RE2A))

#define GPIO_RE2B_AS_INPUT()	(GPIO_RE2B_DDR &= ~(1 << GPIO_RE2B))
#define GPIO_RE2B_PULLUP_ENABLE()	(GPIO_RE2B_PORT |= (1 << GPIO_RE2B))
#define GPIO_RE2B_READ()	(GPIO_RE2B_PIN & (1 << GPIO_RE2B))

#define GPIO_LED1_AS_OUTPUT()	(GPIO_LED1_DDR |= (1 << GPIO_LED1))
#define GPIO_LED1_OFF()		(GPIO_LED1_PORT &= ~(1 << GPIO_LED1))
#define GPIO_LED1_ON()		(GPIO_LED1_PORT |= (1 << GPIO_LED1))

#define GPIO_LED2_AS_OUTPUT()	(GPIO_LED2_DDR |= (1 << GPIO_LED2))
#define GPIO_LED2_OFF()		(GPIO_LED2_PORT &= ~(1 << GPIO_LED2))
#define GPIO_LED2_ON()		(GPIO_LED2_PORT |= (1 << GPIO_LED2))

#define GPIO_LED3_AS_OUTPUT()	(GPIO_LED3_DDR |= (1 << GPIO_LED3))
#define GPIO_LED3_OFF()		(GPIO_LED3_PORT &= ~(1 << GPIO_LED3))
#define GPIO_LED3_ON()		(GPIO_LED3_PORT |= (1 << GPIO_LED3))

#define GPIO_LED4_AS_OUTPUT()	(GPIO_LED4_DDR |= (1 << GPIO_LED4))
#define GPIO_LED4_OFF()		(GPIO_LED4_PORT &= ~(1 << GPIO_LED4))
#define GPIO_LED4_ON()		(GPIO_LED4_PORT |= (1 << GPIO_LED4))

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void GPIO_Init(void)
{
	//
	// Setup input pins.
	//

	GPIO_RE1A_AS_INPUT();
	GPIO_RE1B_AS_INPUT();
	GPIO_RE2A_AS_INPUT();
	GPIO_RE2B_AS_INPUT();

	GPIO_RE1A_PULLUP_ENABLE();
	GPIO_RE1B_PULLUP_ENABLE();
	GPIO_RE2A_PULLUP_ENABLE();
	GPIO_RE2B_PULLUP_ENABLE();

	//
	// Setup output pins.
	//

	GPIO_LED1_AS_OUTPUT();
	GPIO_LED2_AS_OUTPUT();
	GPIO_LED3_AS_OUTPUT();
	GPIO_LED4_AS_OUTPUT();

	GPIO_LED1_ON();
	GPIO_LED2_ON();
	GPIO_LED3_ON();
	GPIO_LED4_ON();
}

uint8_t GPIO_ReadInputs(void)
{
	uint8_t rv;

	rv = 0;

	if(GPIO_RE1A_READ())	rv |= (1 << 0);
	if(GPIO_RE1B_READ())	rv |= (1 << 1);
	if(GPIO_RE2A_READ())	rv |= (1 << 2);
	if(GPIO_RE2B_READ())	rv |= (1 << 3);

	return rv;
}

void GPIO_WriteOutputs(uint8_t byte)
{
	if(byte & (1 << 0))	GPIO_LED1_ON();
	else			GPIO_LED1_OFF();

	if(byte & (1 << 1))	GPIO_LED2_ON();
	else			GPIO_LED2_OFF();

	if(byte & (1 << 2))	GPIO_LED3_ON();
	else			GPIO_LED3_OFF();

	if(byte & (1 << 3))	GPIO_LED4_ON();
	else			GPIO_LED4_OFF();

}

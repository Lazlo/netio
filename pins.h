#ifndef D_PINS_H
#define D_PINS_H

#include <avr/io.h>

///////////////////////////////////////////////////////////////////////////////
//
// Encoders
//
///////////////////////////////////////////////////////////////////////////////

#define GPIO_RE1A	2
#define GPIO_RE1A_DDR	DDRD
#define GPIO_RE1A_PORT	PORTD
#define GPIO_RE1A_PIN	PIND

#define GPIO_RE1B	3
#define GPIO_RE1B_DDR	DDRD
#define GPIO_RE1B_PORT	PORTD
#define GPIO_RE1B_PIN	PIND

#define GPIO_RE2A	4
#define GPIO_RE2A_DDR	DDRD
#define GPIO_RE2A_PORT	PORTD
#define GPIO_RE2A_PIN	PIND

#define GPIO_RE2B	5
#define GPIO_RE2B_DDR	DDRD
#define GPIO_RE2B_PORT	PORTD
#define GPIO_RE2B_PIN	PIND

///////////////////////////////////////////////////////////////////////////////
//
// LEDs
//
///////////////////////////////////////////////////////////////////////////////

#define GPIO_LED1	6
#define GPIO_LED1_DDR	DDRD
#define GPIO_LED1_PORT	PORTD

#define GPIO_LED2	7
#define GPIO_LED2_DDR	DDRD
#define GPIO_LED2_PORT	PORTD

#define GPIO_LED3	0
#define GPIO_LED3_DDR	DDRB
#define GPIO_LED3_PORT	PORTB

#define GPIO_LED4	3
#define GPIO_LED4_DDR	DDRB
#define GPIO_LED4_PORT	PORTB

#endif // D_PINS_H

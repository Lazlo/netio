#ifndef D_ROTENC_H
#define D_ROTENC_H

#include <inttypes.h>

void Enc_Init(void);

void Enc_Update(void);

void Enc_FeedData(uint8_t data);

int Enc_Changed(void);

int Enc_GetKey(void);

#endif // D_ROTENC_H

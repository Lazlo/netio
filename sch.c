//! \file sch.c
//! \brief Co-operative task scheduler

//
// Cooperative Task Scheduler
//

// This library provides an implementation of a cooperative task scheduler,
// entirely written in the C programming language, targeted for use in embedded systems.
//
// In order to use the scheduler, a hardware timer is required to cause periodic interrupts.
// A timer interupt will cause the scheduler to inspect its task list (by calling its Sch_Update()
// function) and check if a task is up to be run.
//
// The execution of tasks in then performed in a super loop, in the main execution context
// of the systems firmware (using the Sch_Dispatch_Tasks() function).
//
// The scheduler interface is designed to allow setting up tasks for execution,
// either periodic or single-shot (using the Sch_Add_Task() function).

// Sch_Init()		-- initialize scheduler
// Sch_Add_Task()	-- add task to scheduler task list
// Sch_Delete_Task()	-- delete task from task list
// Sch_Start()		-- enable the scheduler
// Sch_Update()		-- chech for tasks to be executed
// Sch_Dispatch_Tasks()	-- execute tasks
// Sch_Report()		-- get error/status code

// Example usage:
//
// #include "sch.h"
//
// Sch_Init();
//
// Sch_Add_Task(TaskFoo, 0,  500);
// Sch_Add_Task(TaskBar, 0, 1000);
// Sch_Add_Task(TaskBlub, 100, 0);
//
// Sch_Start();
//
// while(1)
// {
//	Sch_Dispatch_Tasks();
// }
//
// ISR()
// {
//	Sch_Update();
// }

///////////////////////////////////////////////////////////////////////////////
//
// Includes
//
///////////////////////////////////////////////////////////////////////////////

#include <inttypes.h>

#include <avr/interrupt.h>
#include <avr/sleep.h>

#include "sch.h"
#include "timer.h"

///////////////////////////////////////////////////////////////////////////////
//
// Macro constants
//
///////////////////////////////////////////////////////////////////////////////

//! \brief Number of tasks that can be scheduled.
#define SCH_TASKS_MAX 4

///////////////////////////////////////////////////////////////////////////////
//
// Custom data types
//
///////////////////////////////////////////////////////////////////////////////

typedef struct data
{
	void (* fp)(void);

	uint16_t delay;
	uint16_t periode;

	uint8_t run;
} TaskStruct_t;

///////////////////////////////////////////////////////////////////////////////
//
// Private variables
//
///////////////////////////////////////////////////////////////////////////////

// List of tasks.
static TaskStruct_t s_task[SCH_TASKS_MAX];

// Number of tasks installed.
static TaskId_t s_tasks_installed;

///////////////////////////////////////////////////////////////////////////////
//
// Private functions
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// Public functions
//
///////////////////////////////////////////////////////////////////////////////

//! \brief Initialize scheduler data structure and hardware timer.
//! \param tick_ms Time in miliseconds to be used as the tick interval.
//!
void Sch_Init(const uint16_t tick_ms)
{
	TaskId_t i;

	s_tasks_installed = 0;

	// Initialize task list with empty tasks.
	for(i = 0; i < SCH_TASKS_MAX; i++)
		Sch_Delete_Task(i);

	// Initialize timer1 device to tick every N miliseconds.
	Timer1_Init(tick_ms);
}

//! \brief Add a task to the scheduler.
//! \param (*fp)() Pointer to task function to be scheduled.
//! \param delay Delay in ticks before the task is marked to run.
//! \param periode Periode in ticks when task is to be executed periodically.
//! \return The id of the added tasks or SCH_TASKS_MAX when adding the task failed.
//!
TaskId_t Sch_Add_Task(void (* fp)(void), const unsigned int delay, const unsigned int periode)
{
	TaskId_t id;

	id = s_tasks_installed;

	// Install task in task list.
	s_task[id].fp = fp;
	s_task[id].delay = delay;
	s_task[id].periode = periode;
	s_task[id].run = 0;

	// Increment number of tasks installed.
	s_tasks_installed++;

	return id;
}

//! \brief Delete a task.
//! \param id Numeric identifier for task to be deleted.
void Sch_Delete_Task(const TaskId_t id)
{
	s_task[id].fp = 0;
	s_task[id].delay = 0;
	s_task[id].periode = 0;
	s_task[id].run = 0;
}

//! \brief Start the scheduler.
//!
//! WHY DOES THIS NOT SHOW IN DOXYGEN?!
void Sch_Start(void)
{
	// Enable interrupts globally.
	sei();
}

// This function is to be called from the ISR.
static void Sch_Update(void)
{
	uint8_t x;

	for(x = 0; x < s_tasks_installed; x++)
	{
		// Check if there is valid entry (function pointer is not null).
		if(s_task[x].fp)
		{
			// Check if task delay is up for execution.
			if(s_task[x].delay == 0)
			{
				// Reset delay
				s_task[x].delay = s_task[x].periode;

				// Mark task for execution.
				s_task[x].run += 1;
			}
			else
			{
				// Task not ready to run. Decrement delay value by one.
				s_task[x].delay -= 1;
			}
		}
	}
}

//! \brief Execute tasks marked waiting.
//!
//! This function should be called from inside the super loop in the main() function.
void Sch_Dispatch_Tasks(void)
{
	uint8_t x;

	for(x = 0; x < s_tasks_installed; x++)
	{
		if(s_task[x].run)
		{
			// Execute function.
			(*s_task[x].fp)();

			// Clear run flag.
			s_task[x].run -= 1;

		}
	}

	// Report status.

	// Go to sleep.
	sleep_mode();
}

//! \brief Timer1 interrupt service routine (ISR).
ISR(TIMER1_COMPA_vect)
{
	Sch_Update();
}

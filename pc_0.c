#include <inttypes.h>

//
// Macro constants for configuration
//

#define LINK_TX_BUF_LENGTH 40

//
// Private variables
//

static uint8_t i_waiting;

static uint8_t i_written;

char tx_buf[LINK_TX_BUF_LENGTH];

void (*s_fptr_putc)(char);

//
// Public functions
//

// Setup function pointer to the function that
// should be called to transmit a charater.
void link_Init(void (fptr_putc)(char))
{
	s_fptr_putc = fptr_putc;
}

void link_Update(void)
{
	if(i_written < i_waiting)
		(*s_fptr_putc)(tx_buf[i_written++]);
	else
		i_waiting = i_written = 0;
}

// Write character to link tx buffer
void link_Putc(const char c)
{
	if(i_waiting < LINK_TX_BUF_LENGTH)
		tx_buf[i_waiting++] = c;
}

// Write string to link tx buffer
void link_Puts(const char *s)
{
	while(*s != '\0')
		link_Putc(*s++);
}

//! \file timer.c
//! \brief Driver for the on-chip timer(s) available on ATMEL AVR micro-controllers.
//!
//! This driver allows to control the on-chip timer 1 device (16-bit), that is
//! part of all ATmega and ATtiny micro-controllers from ATMEL.

#include <inttypes.h>
#include <avr/io.h>

static void t1_set_reload_value(uint16_t reload)
{
	OCR1AH = reload >> 8;
	OCR1AL = reload;
}

static void t1_set_mode_ctc(void) { TCCR1B |= (1 << WGM12); }

static void t1_set_match_interrupt_enable(void) { TIMSK |= (1 << OCIE1A); }

//! \brief Setup timer1 (16-bit) for periodic interrupts.
//! \param ms Time in miliseconds between two interrupts.
//!
void Timer1_Init(const uint16_t ms)
{
	uint16_t reload;

	// Calculate timer reload value.
	reload = F_CPU / 1024 / 1000 * ms;

	// Write reload value.
	t1_set_reload_value(reload);

	// Set timer mode to CTC.
	t1_set_mode_ctc();

	// Enable timer compare match interrupt.
	t1_set_match_interrupt_enable();

	// Start timer by selecting a clock source.
	TCCR1B |= (1 << CS12)|(1 << CS10); // F_CPU / 1024
}

//! \file timer.h
//! \brief Driver for the on-chip timer(s) available on ATMEL AVR micro-controllers.

#ifndef D_TIMER_H
#define D_TIMER_H

void Timer1_Init(const uint16_t ms);

#endif // D_TIMER_H

#include "gpio.h"
#include "uart.h"
#include "re.h"
#include "sch.h"
#include "pc_0.h"
#include "uptime.h"

#define UART_BAUD	38400

#define TICK_MS 1

void Task_EncIO(void);
void Task_UpTime(void);
void Task_Link(void);

int main(void)
{
	// Initialize input/output pins.
	GPIO_Init();

	// Initialize serial interface.
	UART_Init(UART_BAUD);

	link_Init(&UART_Putc);

	// Send some output to the serial interface.
	link_Puts("UART ready!\r\n");

	// Initialize uptime module.
	UpTime_Init(0, 0, 0);

	// Initialize scheduler.
	Sch_Init(TICK_MS);

	// Add tasks to be managed by the scheduler.
	Sch_Add_Task(Task_Link,   0,     1);
	Sch_Add_Task(Task_EncIO,  1,    10);
	Sch_Add_Task(Task_UpTime, 2,  1000);

	// Start the scheduler.
	Sch_Start();

	// Loop forever.
	while(1)
	{
		// Let the scheduler work off the tasks.
		Sch_Dispatch_Tasks();
	}

	return 0;
}

void Task_EncIO(void)
{
	uint8_t input_byte;
	uint8_t key;

	// Read byte from GPIOs configured as inputs for rotary encoders.
	input_byte = GPIO_ReadInputs();

	// Write byte to GPIOs configured as outputs for the debug LEDs.
	GPIO_WriteOutputs(input_byte);

	// Feed the data into the rotary encoder library.
	Enc_FeedData(input_byte);

	if(Enc_Changed())
	{
		key = Enc_GetKey();
		link_Puts("re ");
		link_Putc('0'+key);
		link_Putc(' ');

		if(key == 1)
			link_Puts("-->");
		else if(key == 2)
			link_Puts("<--");
		else
			link_Puts("?");

		link_Puts("\r\n");
	}
}

void Task_UpTime(void)
{
	char str[9];

	// Call uptime tick every second.
	UpTime_Tick();
	// Get current uptime as string.
	UpTime_ToStr(str);

	// Write uptime to serial interface.
	link_Puts(str);
	link_Puts(" tick\r\n");
}

void Task_Link(void)
{
	link_Update();
}
